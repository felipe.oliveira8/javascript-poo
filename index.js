class Cliente{
    nome;
    cpf;    
}

class ContaCorrente{
    agencia; 
    //#saldo = 0 proposta de convenção a atributos reservados/privados https://github.com/tc39/proposal-class-fields#private-fields
    _saldo = 0; //Segundo a convenção js, todo atributo com "_" underline na frente simboliza atributo privado
    
    sacar(valor){
        if(this._saldo >= valor){
            this._saldo -= valor;
            console.log("Valor a sacar: "+ this._saldo);
            return valor;
        }else{
            console.log("Saldo insulficiente!");
        } 
    }

        depositar(valor){
            if(valor <= 0)return;
            this._saldo += valor;
            console.log("Valor depositado: " + valor);
        }
    
}


const cliente1 = new Cliente();
cliente1.nome = "Ricardo";
cliente1.cpf = 1122213333;


const cliente2 = new Cliente();
cliente2.nome = "Alice";
cliente2.cpf = 11122211133;

const contaCorrenteRicardo = new ContaCorrente();
contaCorrenteRicardo.agencia = 1001;

contaCorrenteRicardo.depositar(300);
const valorSacado = contaCorrenteRicardo.sacar(50);
console.log('Saque: ' + valorSacado);

console.log(contaCorrenteRicardo);



